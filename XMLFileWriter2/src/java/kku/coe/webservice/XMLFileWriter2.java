package kku.coe.webservice;

import java.io.*;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

public class XMLFileWriter2 {

    public static void main(String argv[]) {

        File file = new File("quotes.xml");

        try {

            // Write an output factory
            XMLOutputFactory outputF = XMLOutputFactory.newInstance();
            // Write an xml stream writer
            XMLStreamWriter streamW = outputF.createXMLStreamWriter(new OutputStreamWriter(
                    new FileOutputStream(file)));

            // Write XML prologue
            streamW.writeStartDocument("UTF-8", "1.0");

            // Create quotes to root element
            streamW.writeStartElement("quotes");
            streamW.writeStartElement("quote");
            streamW.writeStartElement("word");
            streamW.writeCharacters("Time is more value than money. You can get more money, but you cannot get more time");
            streamW.writeEndElement();
            streamW.writeStartElement("by");
            streamW.writeCharacters("Jim Rohn");
            streamW.writeEndElement();
            
            // End element quote
            streamW.writeEndElement();

            streamW.writeStartElement("quote");
            streamW.writeStartElement("word");
            streamW.writeCharacters("เมื่อทำอะไรสำเร็จ แม้จะเป็นเก้าเล้กๆของตัวเอง ก็ควรรู้จักให้รางวัลตัวเองบ้าง");
            streamW.writeEndElement();
            streamW.writeStartElement("by");
            streamW.writeCharacters("ว. วชิรเมธี");
            streamW.writeEndElement();
            
            // End elemrnt quote
            streamW.writeEndElement();

            // End document
            streamW.writeEndDocument();
            streamW.flush();
            streamW.close();
            
            System.out.println("Create quotes.xml Success!!!");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}