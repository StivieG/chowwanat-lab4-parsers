package kku.coe.webservice;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

@WebServlet(name = "FeedWriterForm", urlPatterns = {"/FeedWriterForm"})
public class FeedWriterForm extends HttpServlet {

    Document doc; // filePath is a location of feed file
    String filePath = "C:/Users/StivieG/Documents/NetBeansProjects/FeedWriterForm/web/aa.xml";
    File file = new File(filePath);

    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response)
            throws
            ServletException,
            IOException {
        response.setContentType("text/html;charset=UTF-??8");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out =
                response.getWriter();

        String title = request.getParameter("Title");
        String url = request.getParameter("Link");
        String desc = request.getParameter("description");

        try {
            if (file.exists()) {
                DocumentBuilderFactory builderFactory =
                        DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder =
                        builderFactory.newDocumentBuilder();
                //creatin a new instance of a DoM to build a DOM tree.
                doc = docBuilder.parse(file);
                FeedWriterForm fw = new FeedWriterForm();
                String feed = fw.updateRss(doc, title, url, desc);
                out.print(feed);
            } else {
                DocumentBuilderFactory builderFactory =
                        DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder =
                        builderFactory.newDocumentBuilder();
                doc = docBuilder.newDocument();
                FeedWriterForm fw = new FeedWriterForm();
                String feed = fw.createRss(doc, title, url, desc);
                out.print(feed);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public String createRss(Document doc, String rssTitle, String rssUrl, String rssDescription) throws Exception {
        //root element rss
        Element rss = doc.createElement("rss");

        //attribute version = '2.0'in element rss 
        rss.setAttribute("version", "2.0");
        doc.appendChild(rss);
        //the child element of rss is channel
        Element channel = doc.createElement("channel");
        rss.appendChild(channel);
        //Element / rss / channel / title
        Element title = doc.createElement("title");
        channel.appendChild(title);
        Text titleT = doc.createTextNode("Khon Kean University Rss Feed ");
        title.appendChild(titleT);

        //element/ rss / channel / description
        Element desc = doc.createElement("description");
        channel.appendChild(desc);
        Text descT = doc.createTextNode("Khon Kean University Information News Rss Feed ");
        desc.appendChild(descT);

        //element/rss/channel/link
        Element link = doc.createElement("link");
        channel.appendChild(link);
        Text linkT = doc.createTextNode("http://www.kku.ac.th");
        link.appendChild(linkT);

        //element/rss/channel/lang
        Element lang = doc.createElement("lang");
        channel.appendChild(lang);
        Text langT = doc.createTextNode("en-??th");
        lang.appendChild(langT);

        //element/rss/channel/item
        Element item = doc.createElement("item");
        channel.appendChild(item);

        //element/rss/channel/title
        Element iTitle = doc.createElement("title");
        item.appendChild(iTitle);
        Text iTitleT = doc.createTextNode(rssTitle);
        iTitle.appendChild(iTitleT);
        Element iDesc = doc.createElement("description");
        item.appendChild(iDesc);
        Text iDescT = doc.createTextNode(rssDescription);
        iDesc.appendChild(iDescT);

        Element iLink = doc.createElement("link");
        item.appendChild(iLink);
        Text iLinkT = doc.createTextNode(rssUrl);
        iLink.appendChild(iLinkT);
        Element pubDate = doc.createElement("pubDate");
        item.appendChild(pubDate);
        Text pubDateT = doc.createTextNode((new java.util.Date()).toString());
        pubDate.appendChild(pubDateT);

        //Transformerfactory instance is used to create Transformer objects.
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        //create string from xml tree
        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        String xmlString = sw.toString();

        BufferedWriter bw =
                new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
        bw.write(xmlString);
        bw.flush();
        bw.close();
        return xmlString;
    }

    public String updateRss(Document doc, String title, String url, String desc)
            throws Exception {
        Element channel = (Element) doc.getElementsByTagName("channel").item(0);

        //element/rss/channel/item
        Element item = doc.createElement("item");
        channel.appendChild(item);

        //element/rss/channel/title
        Element iTitle = doc.createElement("title");
        item.appendChild(iTitle);
        Text iTitleT = doc.createTextNode(title);
        iTitle.appendChild(iTitleT);

        //element/rss/channel/description
        Element iDesc = doc.createElement("description");
        item.appendChild(iDesc);
        Text iDescT = doc.createTextNode(desc);
        iDesc.appendChild(iDescT);

        //element/rss/channel/link
        Element iLink = doc.createElement("link");
        item.appendChild(iLink);
        Text iLinkT = doc.createTextNode(url);
        iLink.appendChild(iLinkT);

        //element/rss/channel/pubDate
        Element pubDate = doc.createElement("pubDate");
        item.appendChild(pubDate);
        Text pubDateT = doc.createTextNode((new java.util.Date()).toString());
        pubDate.appendChild(pubDateT);

        //TransformerFactory instance is used to create transformer objects.
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        //create string from xml tree
        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        String xmlString = sw.toString();

        //set to an appropriate file name in the web project
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
        bw.write(xmlString);
        bw.flush();
        return xmlString;
    }

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response)
            throws
            ServletException,
            IOException {
        processRequest(request,
                response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws
            ServletException,
            IOException {
        processRequest(request,
                response);
    }

    @Override
    public String getServletInfo() {
        return "Short description ";
    }//</editor-??fold>
}
