package kku.coe.webservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLFileSearcher {

    public static void main(String argv[]) throws FileNotFoundException, UnsupportedEncodingException, ParserConfigurationException, SAXException, IOException {
        String keyword;
        // Path of the input text file
        String keywordFile = "keyword.txt";
        File file = new File("quotes.xml");
        Scanner scan = new Scanner(new FileInputStream(keywordFile), "UTF-8");

        try {
            while (scan.hasNextLine()) {
                keyword = scan.nextLine();
                
                DocumentBuilderFactory builderFact = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = builderFact.newDocumentBuilder();
                Document doc = builder.parse(file);
                //doc.getDocumentElement().normalize();

                // Get element with name quote
                NodeList quotesE = doc.getElementsByTagName("quote");

                for (int i = 0; i < quotesE.getLength(); i++) {
                    Element item = (Element) quotesE.item(i);

                    // Search for quote that has author with the given keyword
                    if (getElemVal(item, "by").toLowerCase().contains(keyword.toLowerCase())) {
                        // Get content with element word and element by
                        System.out.println(getElemVal(item, "word") + " by " + getElemVal(item, "by"));
                    }
                }
            }
        } finally {
            scan.close();
        }
    }

    // Method getElemVal is get content with given element
    protected static String getElemVal(Element parent, String lable) {
        Element e = (Element) parent.getElementsByTagName(lable).item(0);
        try {
            Node child = e.getFirstChild();
            if (child instanceof CharacterData) {
                CharacterData cData = (CharacterData) child;
                return cData.getData();
            }
        } catch (Exception ex) {
        }
        return "";
    }
    
}