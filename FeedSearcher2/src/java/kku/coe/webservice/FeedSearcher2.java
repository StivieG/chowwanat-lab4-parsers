package kku.coe.webservice;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.net.*;
import javax.xml.stream.*;
import javax.xml.stream.events.*;
import java.io.*;
import javassist.compiler.ast.Keyword;
import javax.servlet.annotation.WebServlet;

@WebServlet(name = "FeedSearcher2", urlPatterns = {"/FeedSearcher2"})
public class FeedSearcher2 extends HttpServlet {

    XMLEventReader reader;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        String inputURL = request.getParameter("url");
        String inputKeyword = request.getParameter("keyword");
        boolean titleFound = false;
        boolean linkFound = false;
        boolean itemFound = false;
        String title = null;
        String link = null;
        String eName = null;

        try {
            URL u = new URL(inputURL);
            InputStream in = u.openStream();
            XMLInputFactory factory = XMLInputFactory.newInstance();

            reader = factory.createXMLEventReader(in);
            out.print(
                    "<html><body><table border = '1'><tr ><th>Title</th><th>Link</th ></tr>");
            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();
                if (event.isStartElement()) {
                    StartElement element = (StartElement) event;
                    eName = element.getName().getLocalPart();

                    if (eName.equals("item")) {
                        itemFound = true;
                    }
                    if (itemFound && eName.equals("link")) {
                        linkFound = true;
                    }
                    if (itemFound && eName.equals("title")) {
                        titleFound = true;
                    }
                }
                if (event.isEndElement()) {
                    EndElement element = (EndElement) event;
                    eName = element.getName().getLocalPart();
                    if (eName.equals("item")) {
                        itemFound = false;
                    }
                    if (itemFound && eName.equals("link")) {
                        linkFound = false;
                    }
                    if (itemFound && eName.equals("title")) {
                        titleFound = false;
                    }
                }
                if (event.isCharacters()) {
                    Characters characters = (Characters) event;

                    if (titleFound) {
                        title = characters.getData();
                    }

                    if (linkFound) {
                        link = characters.getData();

                        if (title.toLowerCase().contains(inputKeyword.toLowerCase())) {
                            out.print("<tr><td>");
                            out.print(title);
                            out.print("</td>");

                            out.print("<td>");
                            out.print("<a href='" + link + "' >" + link + "</a>");
                            out.print("</td></tr>");
                        }
                    }
                }
            }
            // end while
            reader.close();
            out.print("</table></body></html>");
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        } finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short   description";
    }
}