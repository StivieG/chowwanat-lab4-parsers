package kku.coe.webservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.Scanner;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class XMLFileSearcher2 {

    public static void main(String argv[]) throws FileNotFoundException , XMLStreamException {
        String keywordFile = "keyword.txt";
        File quoteFile = new File("quotes.xml");
        boolean quoteFound = false;
        boolean wordFound = false;
        boolean byFound = false;
        String keyword = null;
        String word = null;
        String by = null;
        String eName = null;
        Scanner scan = new Scanner(new FileInputStream(keywordFile),"UTF-8");
        
        try {
            while (scan.hasNext()) 
            {
            keyword = scan.nextLine();
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLEventReader reader = factory.createXMLEventReader(new InputStreamReader(new FileInputStream(quoteFile)));
            
            while(reader.hasNext()) {
                XMLEvent event = reader.nextEvent();
                
                if (event.isStartElement()) {
                    StartElement startE = (StartElement) event;
                    eName = startE.getName().getLocalPart();
                    if (eName.equals("quote")) {
                        quoteFound = true;
                    }
                    if (eName.equals("word")) {
                        wordFound = true;
                    }
                    if (eName.equals("by")) {
                        byFound = true;
                    }
                 }
                if (event.isEndElement()) {
                        EndElement endE = (EndElement) event;
                        eName = endE.getName().getLocalPart();
                        if (eName.equals("quote")) {
                            quoteFound = false;
                        }
                        if (quoteFound && eName.equals("word")) {
                            wordFound = false;
                        }
                        if (quoteFound && eName.equals("by")) {
                            byFound = false;
                        }
                    }
                if (event.isCharacters()) {
                        Characters character = (Characters) event;

                        if(byFound) {
                            by = character.getData();
                            
                            if(by.toLowerCase().contains(keyword.toLowerCase())) {
                                // 
                                System.out.println(word + " by " + by);
                            }
                        }
                        if(wordFound) {
                            word = character.getData();
                        }
                    }
                }
            }
         } finally {
           scan.close();
        }
     }
  }