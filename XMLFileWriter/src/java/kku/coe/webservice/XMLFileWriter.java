package kku.coe.webservice;

import java.io.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;

public class XMLFileWriter {

    public static void main(String argv[]) {

        File file = new File("quotes.xml");

        try {

            DocumentBuilderFactory builderFact = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFact.newDocumentBuilder();

            Document doc = builder.newDocument();

            // Create quotes to root element
            Element rootElement = doc.createElement("quotes");
            doc.appendChild(rootElement);

            // Create first quote element 
            Element quoteE = doc.createElement("quote");
            rootElement.appendChild(quoteE);

            Element wordE = doc.createElement("word");
            wordE.appendChild(doc.createTextNode("Time is more value than money. You can get more money, but you cannot get more time"));
            quoteE.appendChild(wordE);

            Element byE = doc.createElement("by");
            byE.appendChild(doc.createTextNode("Jim Rohn"));
            quoteE.appendChild(byE);

            // Create second quote element 
            Element quoteE2 = doc.createElement("quote");
            rootElement.appendChild(quoteE2);

            Element word2E = doc.createElement("word");
            word2E.appendChild(doc.createTextNode("เมื่อทำอะไรสำเร็จ แม้จะเป็นเก้าเล้กๆของตัวเอง ก็ควรรู้จักให้รางวัลตัวเองบ้าง"));
            quoteE2.appendChild(word2E);

            Element byE2 = doc.createElement("by");
            byE2.appendChild(doc.createTextNode("ว. วชิรเมธี"));
            quoteE2.appendChild(byE2);

            TransformerFactory ttf = TransformerFactory.newInstance();
            Transformer tf = ttf.newTransformer();
            tf.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource ds = new DOMSource(doc);
            StreamResult sr = new StreamResult(file);

            tf.transform(ds, sr);

            System.out.println("Create quotes.xml Success!!!");

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}