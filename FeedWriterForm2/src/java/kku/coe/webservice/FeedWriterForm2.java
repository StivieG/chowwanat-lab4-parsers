package kku.coe.webservice;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.xml.stream.*;
import javax.xml.stream.events.*;

@WebServlet(name = "FeedWriterForm2", urlPatterns = {"/FeedWriterForm2"})
public class FeedWriterForm2 extends HttpServlet {

    // filePath is a location of feed file
    String filePath = "C:/Users/StivieG/Documents/NetBeansProjects/FeedWriterForm2/wii.xml";
    File file = new File(filePath);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Set encoding of request data is UTF-8
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        try {
            // Get title, url and description from index.jsp
            String title = request.getParameter("Title");
            String link = request.getParameter("Link");
            String desc = request.getParameter("description");

            if (file.exists()) {
                // If the feed file exits, the lastest filled information
                // is appended to the existing file
                new FeedWriterForm2().updateRssFeed(title, desc, link);
            } else {
                // If the feed file does not exits, the programe creates
                // a new feed file
                new FeedWriterForm2().createRssFeed(title, desc, link);
            }

            out.println("<b <a href='http://localhost:8080/FeedWriterForm2/wii.xml'>" + "Rss Feed</a> was create successfully</b>");

        } catch (Exception e) {
            System.out.println();
        }
    }

    private void createRssFeed(String title, String desc, String link) throws Exception {
        // Create an output factory
        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        // Create an XML stream writer
        XMLStreamWriter xtw = xof.createXMLStreamWriter(new OutputStreamWriter(
                new FileOutputStream(file)));

        // Write XML prologue
        xtw.writeStartDocument();
        xtw.writeStartElement("rss");
        xtw.writeAttribute("version", "2.0");
        xtw.writeStartElement("channel");
        xtw.writeStartElement("title");
        xtw.writeCharacters("Khon Kean University RSS Feed");
        xtw.writeEndElement();
        //end title
        xtw.writeStartElement("description");
        xtw.writeCharacters("Khon Kean University Information News RSS Feed");
        xtw.writeEndElement();
        //end description
        xtw.writeStartElement("link");
        xtw.writeCharacters("http://www.kku.ac.th");
        xtw.writeEndElement();
        //end link
        xtw.writeStartElement("lang");
        xtw.writeCharacters("en~th");
        xtw.writeEndElement();
        //end lang

        //element/rss/channel/item
        xtw.writeStartElement("item");
        //element/rss/channel/item/title
        xtw.writeStartElement("title");
        xtw.writeCharacters(title);
        xtw.writeEndElement();
        //element/rss/channel/item/description
        xtw.writeStartElement("description");
        xtw.writeCharacters(desc);
        xtw.writeEndElement();
        //element/rss/channel/item/link
        xtw.writeStartElement("link");
        xtw.writeCharacters(link);
        xtw.writeEndElement();
        //element/rss/channel/item/pubDate
        xtw.writeStartElement("pubDate");
        xtw.writeCharacters((new java.util.Date()).toString());
        xtw.writeEndElement();
        xtw.writeEndElement();
        //end element item
        xtw.writeEndElement();
        //end element channel
        xtw.writeEndElement();
        //end element rss
        xtw.writeEndDocument();
        xtw.flush();
        xtw.close();
    }

    private void updateRssFeed(String title, String desc, String link) throws Exception {
        // Create object in class XMLInputFactory
        XMLInputFactory xif = XMLInputFactory.newInstance();
        // Create parser object in class XMLEventReader
        XMLEventReader feedReader = xif.createXMLEventReader(new InputStreamReader(new FileInputStream(file)));
        // Create parser object in class XMLOutputFactory
        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        // Create parser object in class XMLStreamWriter
        XMLStreamWriter feedWriter = xof.createXMLStreamWriter(new OutputStreamWriter(new FileOutputStream(file)));
        String elementName;

        // Iterate until there is no more data to read
        while (feedReader.hasNext()) {
            // Get the next XMLEvent
            XMLEvent event = feedReader.nextEvent();

            // Found start document
            if (event.getEventType() == XMLEvent.START_DOCUMENT) {
                // Write XML prologue
                feedWriter.writeStartDocument();
            }

            // If this part of data is the start tag
            if (event.getEventType() == XMLEvent.START_ELEMENT) {
                StartElement element = (StartElement) event;
                elementName = element.getName().getLocalPart();
                if (elementName.equals("rss")) {
                    feedWriter.writeStartElement("rss");
                    feedWriter.writeAttribute("version", "2.0");
                } else {
                    feedWriter.writeStartElement(elementName);
                }
            }

            // Found end element
            if (event.isEndElement()) {
                EndElement element = (EndElement) event;
                elementName = element.getName().getLocalPart();
                // Appended item into channel when found element channel
                if (elementName.equals("channel")) {
                    //element/rss/channel/item
                    feedWriter.writeStartElement("item");
                    //element/rss/channel/item/title
                    feedWriter.writeStartElement("title");
                    feedWriter.writeCharacters(title);
                    feedWriter.writeEndElement();
                    //element/rss/channel/item/description
                    feedWriter.writeStartElement("description");
                    feedWriter.writeCharacters(desc);
                    feedWriter.writeEndElement();
                    //element/rss/channel/item/link
                    feedWriter.writeStartElement("link");
                    feedWriter.writeCharacters(link);
                    feedWriter.writeEndElement();
                    //element/rss/channel/item/pubDate
                    feedWriter.writeStartElement("pubDate");
                    feedWriter.writeCharacters((new java.util.Date()).toString());
                    feedWriter.writeEndElement();
                    feedWriter.writeEndElement();
                    feedWriter.writeEndDocument();
                    feedWriter.flush();
                    feedWriter.close();
                    return;
                } else {
                    feedWriter.writeEndElement();
                }
            }
            
            // If this part of data is characters section
            if (event.isCharacters()) {
                Characters characters = (Characters) event;
                feedWriter.writeCharacters(characters.getData().toString());
            }
        }

        feedWriter.flush();
        feedWriter.close();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}

